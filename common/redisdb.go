package common

import "github.com/go-redis/redis/v8"

type RedisClient struct {
	client *redis.Client
}

var rdb *RedisClient

func RedisInit(addres string, password string) *RedisClient {
	cl := redis.NewClient(&redis.Options{
		Addr:     addres,
		Password: password, // no password set
		DB:       0,        // use default DB
	})
	return &RedisClient{
		client: cl,
	}
}

func GetRedis() *redis.Client {
	return rdb.client
}
