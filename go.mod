module train

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.4 // indirect
	github.com/go-redis/redis/v8 v8.11.3 // indirect
	github.com/prometheus/client_golang v1.11.0 // indirect
	go.mongodb.org/mongo-driver v1.7.2 // indirect
)
