package main

import (
	"context"
	"net/http"

	"train/common"
	"train/contact"

	"github.com/go-chi/chi/v5"

	pr "train/prometheus"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var ctx = context.Background()

// func recordMetrics() {
// 	go func() {
// 		for {
// 			opsProcessed.Inc()
// 			time.Sleep(2 * time.Second)
// 		}
// 	}()
// }

// var (
// 	opsProcessed = promauto.NewCounter(prometheus.CounterOpts{
// 		Name: "myapp_processed_ops_total",
// 		Help: "The total number of processed events",
// 	})
// )

func registerRoutes() http.Handler {
	var h *contact.Handler
	r := chi.NewRouter()
	m := pr.NewMiddleware("serviceName")
	r.Use(m)
	// recordMetrics()
	r.Handle("/metrics", promhttp.Handler())
	r.Route("/contacts", func(r chi.Router) {
		r.Get("/", h.GetAllContact)                 //GET /contacts
		r.Get("/{phonenumber}", h.GetContact)       //GET /contacts/0147344454
		r.Post("/", h.AddContact)                   //POST /contacts
		r.Put("/{phonenumber}", h.UpdateContact)    //PUT /contacts/0147344454
		r.Delete("/{phonenumber}", h.DeleteContact) //DELETE /contacts/0147344454
	})
	return r
}

func main() {
	common.MongoInit()
	common.RedisInit("localhost:6379", "")
	var r = registerRoutes()
	http.ListenAndServe(":3000", r)
}
