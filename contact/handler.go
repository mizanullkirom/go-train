package contact

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-redis/redis/v8"
	"go.mongodb.org/mongo-driver/bson"
)

type Handler struct{}

var ctx = context.Background()

// GetAllContact is fucntion to get all contact in db
func (h *Handler) GetAllContact(w http.ResponseWriter, r *http.Request) {
	contacts := Get(bson.M{})
	json.NewEncoder(w).Encode(contacts)
}

func (h *Handler) GetContact(w http.ResponseWriter, r *http.Request) {
	phoneNumber := chi.URLParam(r, "phonenumber")
	if phoneNumber == "" {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	contact := &Contact{}
	rdb := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	//singleton still get error for redis
	// rdb := common.GetRedis()

	val, err := rdb.Get(ctx, phoneNumber).Result()
	if err == redis.Nil {
		err := GetOne(contact, bson.M{"phoneNumber": phoneNumber})
		if err != nil {
			http.Error(w, fmt.Sprintf("Contact with phonenumber: %s not found", phoneNumber), 404)
			return
		}
		b, err := json.Marshal(contact)
		if err != nil {
			fmt.Println(err)
			return
		}
		errSet := rdb.Set(ctx, phoneNumber, string(b), 0).Err()
		if errSet != nil {
			panic(errSet)
			return
		}
		json.NewEncoder(w).Encode(contact)
	} else if err != nil {
		http.Error(w, fmt.Sprint(err), 400)
		return
	} else {
		w.Write([]byte(fmt.Sprintf(val)))
		return
	}
}

func (h *Handler) AddContact(w http.ResponseWriter, r *http.Request) {
	existingContact := &Contact{}
	var contact Contact
	json.NewDecoder(r.Body).Decode(&contact)
	contact.CreatedOn = time.Now()
	err := GetOne(existingContact, bson.M{"phoneNumber": contact.PhoneNumber})
	if err == nil {
		http.Error(w, fmt.Sprintf("Contact with phonenumber: %s already exist", contact.PhoneNumber), 400)
		return
	}
	_, err = AddOne(&contact)
	if err != nil {
		http.Error(w, fmt.Sprint(err), 400)
		return
	}
	w.Write([]byte("Contact created successfully"))
	w.WriteHeader(201)
}

func (h *Handler) DeleteContact(w http.ResponseWriter, r *http.Request) {
	existingContact := &Contact{}
	phoneNumber := chi.URLParam(r, "phonenumber")
	if phoneNumber == "" {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	err := GetOne(existingContact, bson.M{"phoneNumber": phoneNumber})
	if err != nil {
		http.Error(w, fmt.Sprintf("Contact with phonenumber: %s does not exist", phoneNumber), 400)
		return
	}
	_, err = RemoveOne(bson.M{"phoneNumber": phoneNumber})
	if err != nil {
		http.Error(w, fmt.Sprint(err), 400)
		return
	}
	w.Write([]byte("Contact deleted"))
	w.WriteHeader(200)
}

func (h *Handler) UpdateContact(w http.ResponseWriter, r *http.Request) {
	phoneNumber := chi.URLParam(r, "phonenumber")
	if phoneNumber == "" {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	contact := &Contact{}
	json.NewDecoder(r.Body).Decode(contact)
	_, err := Update(bson.M{"phoneNumber": phoneNumber}, contact)
	if err != nil {
		http.Error(w, fmt.Sprint(err), 400)
		return
	}
	w.Write([]byte("Contact update successful"))
	w.WriteHeader(200)
}
