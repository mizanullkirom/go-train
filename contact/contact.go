package contact

import (
	"time"
)

type Contact struct {
	FirstName   string    `json:"firstName" bson:"firstName"`
	LastName    string    `json:"lastName" bson:"lastName"`
	Email       string    `json:"email" bson:"email"`
	PhoneNumber string    `json:"phoneNumber" bson:"phoneNumber"`
	Address     string    `json:"address" bson:"address"`
	Company     string    `json:"company" bson:"company"`
	CreatedOn   time.Time `json:"createdOn" bson:"createdon"`
}

// func (c *Contact) GetOne(filter interface{}) error {
// 	mdb := common.GetMongo()
// 	collection := mdb.Collection("contact")
// 	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
// 	err := collection.FindOne(ctx, filter).Decode(c)
// 	return err
// }
