package contact

import (
	"context"
	"log"
	"time"
	"train/common"

	"go.mongodb.org/mongo-driver/mongo"
)

func GetOne(c *Contact, filter interface{}) error {
	//Will automatically create a collection if not available
	mdb := common.GetMongo()
	collection := mdb.Collection("contact")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	err := collection.FindOne(ctx, filter).Decode(c)
	return err
}

func Get(filter interface{}) []*Contact {
	mdb := common.GetMongo()
	collection := mdb.Collection("contact")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	cur, err := collection.Find(ctx, filter)

	if err != nil {
		log.Fatal(err)
	}
	defer cur.Close(ctx)

	var result []*Contact
	for cur.Next(ctx) {
		contact := &Contact{}
		er := cur.Decode(contact)
		if er != nil {
			log.Fatal(er)
		}
		result = append(result, contact)
	}
	return result
}

func AddOne(c *Contact) (*mongo.InsertOneResult, error) {
	mdb := common.GetMongo()
	collection := mdb.Collection("contact")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result, err := collection.InsertOne(ctx, c)
	return result, err
}

func Update(filter interface{}, update interface{}) (*mongo.UpdateResult, error) {
	mdb := common.GetMongo()
	collection := mdb.Collection("contact")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result, err := collection.UpdateMany(ctx, filter, update)
	return result, err
}

func RemoveOne(filter interface{}) (*mongo.DeleteResult, error) {
	mdb := common.GetMongo()
	collection := mdb.Collection("contact")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	result, err := collection.DeleteOne(ctx, filter)
	return result, err
}
